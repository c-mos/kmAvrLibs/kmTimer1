/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//** @file
* @mainpage
* @section pageTOC Content
* @brief The kmTimer1 library provides functions to initialize and control Timer 1 on AVR MCUs.
* - kmTimer1.h
* - kmTimer1DefaultConfig.h
*
*  **Created on**: Dec 10, 2023 @n
*      **Author**: Krzysztof Moskwa @n
*      **License**: GPL-3.0-or-later @n@n
*
*  kmDebug library for AVR MCUs @n
*  **Copyright (C) 2022  Krzysztof Moskwa**
*  
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef KM_TIMER_1_H_
#define KM_TIMER_1_H_

#ifdef KM_DOXYGEN
#define COM1C0
#endif /* KM_DOXYGEN */

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>

#include "../kmCommon/kmCommon.h"
#include "../kmTimersCommon/kmTimerDefs.h"

/// Remapping macro for saving own user data in callback registration #kmTimer1RegisterCallbackCompA function
#define KM_TIMER1_USER_DATA(X) (void *)(X)

/**
Definition of the Timer1 Callback Type
@param Pointer for void user data content that is used in #kmTimer1RegisterCallbackCompA function
and will be delivered to callback."as is"
*/
typedef void kmTimer1CallbackType(void *);

/**
@brief Callback type for Timer 1 input capture events.

This is the function pointer type for the callback function that will be
called when a Timer 1 input capture event occurs.

@param[out] captureValue The captured value from the Timer 1 input capture.
@param[out] captureState The input state information obtained during the capture event.
@param[out] userData A pointer to user-defined data that was registered with the callback.

@note The callback function must adhere to this signature to be compatible
      with the Timer 1 input capture system.
*/
typedef void kmTimer1InpCaptureCallbackType(const uint16_t, const bool, const void *);

/**
Enumeration type definition of the PWM outputs for timer used in parameters of #kmTimer1SetPwmDuty, #kmTimer1SetPwmDuty, #kmTimer1SetPwmInversion functions .
*/
typedef enum {
	/// PWM on output A.
	KM_TCC1_PWM_OUT_A,
	/// PWM on output B.
	KM_TCC1_PWM_OUT_B,
#ifdef COM1C0
	/// PWM on output C.
	KM_TCC1_PWM_OUT_C
#endif /* COM1C0 */
} Tcc1PwmOut;

/**
@brief Initializes Timer 1 in a mode allowing the use of CompA, CompB, CompC, and Overflow Interrupts launched at a specific phase of the timer.

The timer always counts from 0 to KM_\b TIMER1_MAX value. Callbacks for CompA, CompB, and CompC are launched
once the timer reaches specific values set by #kmTimer1SetValueCompA, #kmTimer1SetValueCompB, and #kmTimer1SetValueCompC.
The callbacks need to be configured with #kmTimer1RegisterCallbackOVF, #kmTimer1RegisterCallbackCompA,
#kmTimer1RegisterCallbackCompB, and #kmTimer1RegisterCallbackCompC functions. The period/frequency of each of these Interrupts
will be exactly the same and defined by MCU main clock and the prescaler.
The Overflow callback is called when Timer 1 reaches the value \b KM_TIMER1_MAX.
CompA, CompB, and CompC callbacks are called when Timer 1 reaches values set by #kmTimer1SetValueCompA, #kmTimer1SetValueCompB, and #kmTimer1SetValueCompC respectively.
Setting these values allows shifting callback calls with phase (with accuracy limited by the time that the MCU takes to execute code within the callback).

@param prescaler Selects one of the prescalers as defined in kmTimersCommon/kmTimer1Defs.h (e.g., \b KM_TCC1_PRSC_256).

@code
	kmTimer1InitOnPrescalerBottomToTopOvfCompABCInterruptCallback(KM_TCC0_PRSC_8);

	kmTimer1RegisterCallbackOVF(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_D), callbackOVFToggle);
	kmTimer1EnableInterruptOVF();

	kmTimer1RegisterCallbackCompA(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer1SetValueCompA(KM_TIMER1_TEST_DUTY_25_PERC);
	kmTimer1EnableInterruptCompA();

	kmTimer1RegisterCallbackCompB(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_B), callbackCompBToggle);
	kmTimer1SetValueCompB(KM_TIMER1_TEST_DUTY_50_PERC);
	kmTimer1EnableInterruptCompB();
	kmTimer1RegisterCallbackCompC(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_C), callbackCompCToggle);
	kmTimer1SetValueCompC(KM_TIMER1_TEST_DUTY_75_PERC);
	kmTimer1EnableInterruptCompC();
	kmTimer1ConfigureOCA(KM_TCC1_A_COMP_OUT_TOGGLE);
	kmTimer1ConfigureOCB(KM_TCC1_B_COMP_OUT_TOGGLE);
	kmTimer1ConfigureOCC(KM_TCC1_C_COMP_OUT_TOGGLE);

	kmTimer1Start();
@endcode

Output from the example code:
\image html kmTimer1Test2.png
 */
void kmTimer1InitOnPrescalerBottomToTopOvfCompABCInterruptCallback(const uint8_t prescaler);

/**
Initializes Timer 1 in the mode allowing to generate frequency with specific period on OCA0 output.
This function tries to configure Timer1 to make the period as accurate as possible -
see limitations in the description of #kmTimer1CalcPerdiod function.
To change period while Timer1 is running calculate new CompA value and use #kmTimer1SetValueCompA and #kmTime0SetPrescale functions.
with kmTimer1CalcPerdiod shifting new periodInMicroseconds by one bit to the right
@param periodInMicroseconds period of the square function on the OCA0, the available range depends on the main MCU clock

Example code:
@code
	kmTimer1InitOnAccuratePeriodGenerateOutputClockA(KM_TIMER1_TEST_1MS);
	kmTimer1Start();
@endcode
Output from the example code:
\image html kmTimer1Test0.png
*/
void kmTimer1InitOnAccuratePeriodGenerateOutputClockA(const uint32_t periodInMicroseconds);

/**
Initializes Timer 1 in the mode allowing to use CompA Interrupt launched every specific period defined in microseconds.
The callback needs to be configured with #kmTimer1RegisterCallbackCompA function.
It's also possible to generate the square wave toggling output with the same period (output frequency is half of frequency of executing callbacks)
Note accuracy is limited with time that used by MCU to execute code within callback.
@param microseconds time period between callback calls defined in microseconds, the available range depends on the main MCU clock

@code
	kmTimer1InitOnAccurateTimeCompAInterruptCallback(KM_TIMER1_TEST_5MS, true);

	kmTimer1RegisterCallbackCompA(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer1EnableInterruptCompA();

	kmTimer1Start();
@endcode
Output from the example code:
\image html kmTimer1Test1.png
*/
void kmTimer1InitOnAccurateTimeCompAInterruptCallback(const uint32_t microseconds, const bool outOnOC1A);

/**
Initializes Timer 1 as a Counter of either rising or falling signal edges on the input T0
@param falling if true the counter will count falling edges, if false - rising edges
*/
void kmTimer1InitExternal(bool falling);

/**
Initializes Timer 1 in the mode allowing to use CompA and CompB Interrupt launched every specific period defined in microseconds.
The CompB Interrupt can be shifted in phase by providing corresponding phaseIntB parameter
The callbacks needs to be configured with #kmTimer1RegisterCallbackCompA and #kmTimer1RegisterCallbackCompB functions.
The timer goes always from 0 to top value defined as calculation for microseconds used as parameter.
This function tries to define period of calling the callbacks as accurate as possible
The range of available frequencies depends on used XTAL\n
\b NOTE: This initialization function is not available on the older models of AVR MCUs (like ATmega8 or ATmega64)

@param microseconds time period between callback calls defined in microseconds, the available range depends on the main MCU clock
@param phaseIntB phase shift between executing CompA and CompB callbacks. The range of the value from 0 to MAX, where 0 means no phase shift and 32768 means 180 phase shift.
The accuracy of the phase shift depends on on the main MCU clock and the microseconds parameter

@code
	kmTimer1InitOnAccurateTimeCompABInterruptCallback(KM_TIMER1_TEST_1MS, KM_TIMER1_TEST_PHASE_45_DEG); // for 1 millisecond

	kmTimer1RegisterCallbackCompA(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer1EnableInterruptCompA();
	kmTimer1RegisterCallbackCompB(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_B), callbackCompBToggle);
	kmTimer1EnableInterruptCompB();

	kmTimer1Start();
@endcode
Output from the example code:
\image html kmTimer1Test5.png
*/
void kmTimer1InitOnAccurateTimeCompABInterruptCallback(const uint32_t microseconds, uint16_t phaseIntB);

/**
Initializes Timer 1 to generate square wave on the OC1x pin of the MCU with specified time period and duty cycle (Fast PWM mode)
To change the duty cycle after initialization #kmTimer1SetPwmDuty function can be used
(only #KM_TCC1_PWM_OUT_B can be used as pwmOut parameter, executing this function KM_TCC1_PWM_OUT_A may cause unpredictable results)\n
OVF, CompA and CompB callback functions can be used with this mode to capture desired moments within Timer 1 periods\n
\b NOTE: This initialization function is not available on the older models of AVR MCUs (like ATmega8 or ATmega64)

@param microseconds time period of the square wave generated on OC1x pin of MCU, the available range and accuracy depends on the main MCU clock
@param dutyA duty of the square wave generated on the OC1A pin of the MCU; the range from 0 to MAX,
where 16384 means 25% and 32768 means 50% duty cycle; the accuracy of the value depends on the main clock of the MCU and \e microseconds parameter,
the function calculates it in the way to be at least equal or higher than #KM_TCC1_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer1DefaultConfig
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC1_MINIMUM_PWM_CYCCLE_ACCURACY\n
\b NOTE: The output on OC1A is activated within this initialization only when this parameter is greater than 0
@param invertedA if true, the output of the square wave generate don OC1A pin is inverted
@param dutyB duty of the square wave generated on the OC1B pin of the MCU; the range from 0 to MAX,
where 16384 means 25% and 32768 means 50% duty cycle; the accuracy of the value depends on the main clock of the MCU and \e microseconds parameter,
the function calculates it in the way to be at least equal or higher than #KM_TCC1_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer1DefaultConfig
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC1_MINIMUM_PWM_CYCCLE_ACCURACY\n
\b NOTE: The output on OC1B is activated within this initialization only when this parameter is greater than 0
@param invertedB if true, the output of the square wave generate don OC1B pin is inverted
@param dutyC Duty of the square wave generated on the OC1C pin of the MCU; the range from 0 to 65535, where 16384 means 25% and 32768 means 50% duty cycle;
the accuracy of the value depends on the main clock of the MCU and microseconds parameter, the function calculates it in the way to be at least equal or higher than
#KM_TCC1_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer1DefaultConfig.
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC1_MINIMUM_PWM_CYCCLE_ACCURACY.
@param invertedC If true, the output of the square wave generated on OC1C pin is inverted.

@code
	uint16_t cyclesRange = kmTimer1InitOnAccurateTimeFastPwm(KM_TIMER1_TEST_1MS, KM_TIMER1_TEST_DUTY_25_PERC, false, KM_TIMER1_TEST_DUTY_50_PERC, false, KM_TIMER1_TEST_DUTY_25_PERC, false);

	kmTimer1RegisterCallbackOVF(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_D), callbackOVFToggle);
	kmTimer1EnableInterruptOVF();
	kmTimer1RegisterCallbackCompA(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer1EnableInterruptCompA();
	kmTimer1RegisterCallbackCompB(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_B), callbackCompBToggle);
	kmTimer1EnableInterruptCompB();
	kmTimer1RegisterCallbackCompC(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_C), callbackCompCToggle);
	kmTimer1EnableInterruptCompC();

	kmTimer1Start();
@endcode
Output from the example code:
\image html kmTimer1Test5.png
*/
#ifdef COM1C0
uint16_t kmTimer1InitOnAccurateTimeFastPwm(const uint32_t microseconds, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB, const uint16_t dutyC, const bool invertedC);
#else /* COM1C0 */
uint16_t kmTimer1InitOnAccurateTimeFastPwm(const uint32_t microseconds, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB);
#endif /* COM1C0 */


/**
Initializes Timer 1 to generate square wave on the OC1x pin of the MCU with specified time period and duty cycle (Phase Correct PWM mode)
To change the duty cycle after initialization #kmTimer1SetPwmDuty function can be used
(only #KM_TCC1_PWM_OUT_B can be used as pwmOut parameter, executing this function KM_TCC1_PWM_OUT_A may cause unpredictable results)\n
OVF, CompA and CompB callback functions can be used with this mode to capture desired moments within Timer 1 periods\n
\b NOTE: This initialization function is not available on the older models of AVR MCUs (like ATmega8 or ATmega64)

@param microseconds time period of the square wave generated on OC1x pin of MCU, the available range and accuracy depends on the main MCU clock
@param dutyA duty of the square wave generated on the OC1A pin of the MCU; the range from 0 to MAX,
where 16384 means 25% and 32768 means 50% duty cycle; the accuracy of the value depends on the main clock of the MCU and \e microseconds parameter,
the function calculates it in the way to be at least equal or higher than #KM_TCC1_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer1DefaultConfig
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC1_MINIMUM_PWM_CYCCLE_ACCURACY\n
\b NOTE: The output on OC1A is activated within this initialization only when this parameter is greater than 0
@param invertedA if true, the output of the square wave generate don OC1A pin is inverted
@param dutyB duty of the square wave generated on the OC1B pin of the MCU; the range from 0 to 65535,
where 16384 means 25% and 32768 means 50% duty cycle; the accuracy of the value depends on the main clock of the MCU and \e microseconds parameter,
the function calculates it in the way to be at least equal or higher than #KM_TCC1_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer1DefaultConfig
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC1_MINIMUM_PWM_CYCCLE_ACCURACY\n
\b NOTE: The output on OC1B is activated within this initialization only when this parameter is greater than 0
@param invertedB if true, the output of the square wave generate don OC1B pin is inverted
@param dutyC Duty of the square wave generated on the OC1C pin of the MCU; the range from 0 to 65535, where 16384 means 25% and 32768 means 50% duty cycle;
the accuracy of the value depends on the main clock of the MCU and microseconds parameter, the function calculates it in the way to be at least equal or higher than
#KM_TCC1_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer1DefaultConfig.
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC1_MINIMUM_PWM_CYCCLE_ACCURACY.
@param invertedC If true, the output of the square wave generated on OC1C pin is inverted.

@code
	uint16_t cyclesRange = kmTimer1InitOnAccurateTimePcPwm(KM_TIMER1_TEST_1MS, KM_TIMER1_TEST_DUTY_25_PERC, false, KM_TIMER1_TEST_DUTY_50_PERC, false, KM_TIMER1_TEST_DUTY_75_PERC, false);

	kmTimer1RegisterCallbackOVF(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_D), callbackOVF);
	kmTimer1EnableInterruptOVF();
	kmTimer1RegisterCallbackCompA(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_A), callbackCompAOff);
	kmTimer1EnableInterruptCompA();
	kmTimer1RegisterCallbackCompB(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_B), callbackCompBOff);
	kmTimer1EnableInterruptCompB();
	kmTimer1RegisterCallbackCompC(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_C), callbackCompCOff);
	kmTimer1EnableInterruptCompC();

	kmTimer1Start();
@endcode
Output from the example code:
\image html kmTimer1Test7.png
*/
#ifdef COM1C0
uint16_t kmTimer1InitOnAccurateTimePcPwm(const uint32_t microseconds, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB, const uint16_t dutyC, const bool invertedC);
#else /* COM1C0 */
uint16_t kmTimer1InitOnAccurateTimePcPwm(const uint32_t microseconds, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB);
#endif /* COM1C0 */


/**
Initializes Timer 1 to generate square wave on the OC1x pin of the MCU with specified duty cycle and frequency defined by prescaler only (Fast PWM mode)
To change the duty cycle after initialization #kmTimer1SetPwmDuty function can be used (for both #KM_TCC1_PWM_OUT_A and #KM_TCC1_PWM_OUT_B)
OVF, CompA and CompB callback functions can be used with this mode to capture desired moments within Timer 1 periods\n
\b NOTE: This initialization function is not available on the older models of AVR MCUs (like ATmega8 or ATmega64)

@param prescaler selects one of prescaler as defined in kmTimersCommon/kmTimer1Defs.h (e.g. KM_TCC1_PRSC_256)
@param dutyA duty of the square wave generated on the OC1A pin of the MCU; the range from 0 to 65535,
where 16384 means 25% and 32768 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle,
\b NOTE: The output on OC1A is activated within this initialization only when this parameter is greater than 0
@param invertedA if true, the output of the square wave generate don OC1A pin is inverted
@param dutyB duty of the square wave generated on the OC1B pin of the MCU; the range from 0 to 65535,
where 16384 means 25% and 32768 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle,
\b NOTE: The output on OC1B is activated within this initialization only when this parameter is greater than 0
@param invertedB if true, the output of the square wave generate don OC1B pin is inverted
@param dutyC Duty of the square wave generated on the OC1C pin of the MCU; the range from 0 to 65535, where 16384 means 25% and 32768 means 50% duty cycle;
the accuracy of the value depends on the main clock of the MCU and microseconds parameter, the function calculates it in the way to be at least equal or higher than
#KM_TCC1_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer1DefaultConfig.
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC1_MINIMUM_PWM_CYCCLE_ACCURACY.
@param invertedC If true, the output of the square wave generated on OC1C pin is inverted.

@code
	kmTimer1InitOnPrescalerBottomToTopFastPwm(KM_TCC1_PRSC_1, KM_TIMER1_TEST_DUTY_25_PERC, false, KM_TIMER1_TEST_DUTY_50_PERC, false, KM_TIMER1_TEST_DUTY_75_PERC, false);

	kmTimer1RegisterCallbackOVF(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_D), callbackOVFToggle);
	kmTimer1EnableInterruptOVF();
	kmTimer1RegisterCallbackCompA(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer1EnableInterruptCompA();
	kmTimer1RegisterCallbackCompB(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_B), callbackCompBToggle);
	kmTimer1EnableInterruptCompB();
	kmTimer1RegisterCallbackCompC(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_C), callbackCompCToggle);
	kmTimer1EnableInterruptCompC();

	kmTimer1Start();
@endcode
Output from the example code:
\image html kmTimer1Test4.png
*/
#ifdef COM1C0
void kmTimer1InitOnPrescalerBottomToTopFastPwm(const uint8_t prescaler, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB, const uint16_t dutyC, const bool invertedC);
#else /* COM1C0 */
void kmTimer1InitOnPrescalerBottomToTopFastPwm(const uint8_t prescaler, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB);
#endif /* COM1C0 */

/**
Initializes Timer 1 to generate square wave on the OC1x pin of the MCU with specified duty cycle and frequency defined by prescaler only (Phase Correct PWM mode)
To change the duty cycle after initialization #kmTimer1SetPwmDuty function can be used (for both #KM_TCC1_PWM_OUT_A and #KM_TCC1_PWM_OUT_B)
OVF, CompA and CompB callback functions can be used with this mode to capture desired moments within Timer 1 periods\n
\b NOTE: This initialization function is not available on the older models of AVR MCUs (like ATmega8 or ATmega64)

@param prescaler selects one of prescaler as defined in kmTimersCommon/kmTimer1Defs.h (e.g. KM_TCC1_PRSC_256)
@param dutyA duty of the square wave generated on the OC1A pin of the MCU; the range from 0 to 65535,
where 16384 means 25% and 32768 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle,
\b NOTE: The output on OC1A is activated within this initialization only when this parameter is greater than 0
@param invertedA if true, the output of the square wave generate don OC1A pin is inverted
@param dutyB duty of the square wave generated on the OC1B pin of the MCU; the range from 0 to 65535,
where 16384 means 25% and 32768 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle,
\b NOTE: The output on OC1B is activated within this initialization only when this parameter is greater than 0
@param invertedB if true, the output of the square wave generate don OC1B pin is inverted
@param dutyC Duty of the square wave generated on the OC1C pin of the MCU; the range from 0 to 65535, where 16384 means 25% and 32768 means 50% duty cycle;
the accuracy of the value depends on the main clock of the MCU and microseconds parameter, the function calculates it in the way to be at least equal or higher than
#KM_TCC1_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer1DefaultConfig.
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC1_MINIMUM_PWM_CYCCLE_ACCURACY.
@param invertedC If true, the output of the square wave generated on OC1C pin is inverted.

@code
	kmTimer1InitOnPrescalerBottomToTopPcPwm(KM_TCC0_PRSC_1, KM_TIMER1_TEST_DUTY_25_PERC, false, KM_TIMER1_TEST_DUTY_50_PERC, false, KM_TIMER1_TEST_DUTY_75_PERC, false);

	kmTimer1RegisterCallbackOVF(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_D), callbackOVF);
	kmTimer1EnableInterruptOVF();
	kmTimer1RegisterCallbackCompA(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_A), callbackCompAOff);
	kmTimer1EnableInterruptCompA();
	kmTimer1RegisterCallbackCompB(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_B), callbackCompBOff);
	kmTimer1EnableInterruptCompB();
	kmTimer1RegisterCallbackCompC(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_C), callbackCompCOff);
	kmTimer1EnableInterruptCompC();

	kmTimer1Start();
@endcode
Output from the example code:
\image html kmTimer1Test6.png
*/
#ifdef COM1C0
void kmTimer1InitOnPrescalerBottomToTopPcPwm(const uint8_t prescaler, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB, const uint16_t dutyC, const bool invertedC);
#else /* COM1C0 */
void kmTimer1InitOnPrescalerBottomToTopPcPwm(const uint8_t prescaler, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB);
#endif /* COM1C0 */

/**
\deprecated Can be used with older models of AVR MCUs like ATmega8, ATmega32, etc.\n
Initializes Timer 1 to generate square wave on the OC1x pin of the MCU with specified duty cycle and frequency defined by prescaler only (Fast PWM mode)
To change the duty cycle after initialization #kmTimer1SetPwmDuty function can be used (#KM_TCC1_PWM_OUT_A can only be used)
OVF and CompA callback functions can be used with this mode to capture desired moments within Timer 1 periods\n

@param prescaler selects one of prescaler as defined in kmTimersCommon/kmTimer1Defs.h (e.g. KM_TCC1_PRSC_256)
@param dutyA duty of the square wave generated on the OC1A pin of the MCU; the range from 0 to 65535,
where 16384 means 25% and 32768 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle,
\b NOTE: The output on OC1A is activated within this initialization only when this parameter is greater than 0
@param invertedA if true, the output of the square wave generate don OC1A pin is inverted
*/
void kmTimer1InitOnPrescalerBottomToTopFastPwmOCA(const uint8_t prescaler, const uint16_t dutyA, const bool invertedA);

/**
\deprecated Can be used with older models of AVR MCUs like ATmega8, ATmega32, etc.\n
InitializesTimer 1 to generate square wave on the OC1x pin of the MCU with specified duty cycle and frequency defined by prescaler only (Phase Accurate mode)
To change the duty cycle after initialization #kmTimer1SetPwmDuty function can be used (#KM_TCC1_PWM_OUT_A can only be used)
OVF and CompA callback functions can be used with this mode to capture desired moments within Timer 1 periods\n

@param prescaler selects one of prescaler as defined in kmTimersCommon/kmTimer1Defs.h (e.g. KM_TCC1_PRSC_256)
@param dutyA duty of the square wave generated on the OC1A pin of the MCU; the range from 0 to 65535,
where 16384 means 25% and 32768 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle,
\b NOTE: The output on OC1A is activated within this initialization only when this parameter is greater than 0
@param invertedA if true, the output of the square wave generate don OC1A pin is inverted
*/
void kmTimer1InitOnPrescalerBottomToTopPcPwmOCA(const uint8_t prescaler, const uint16_t dutyA, const bool invertedA);

/**
@brief Initializes Timer/Counter 1 for input capture with specified parameters.

This function initializes Timer/Counter 1 for input capture mode, configuring it
with the specified parameters. It sets up the necessary callbacks, enables interrupts,
and starts the timer for accurate timing measurements.
The iddle time defines maximum time of captured input state change.

@param iddleTimeMicroseconds The idle time (in microseconds) before capturing signals.
@param iddleUserData User data to be passed to the idle time callback function.
@param iddleCallback Pointer to the callback function for the idle time interrupt.
@param inpCaptureUserData User data to be passed to the input capture callback function.
@param inpCaptureCallback Pointer to the callback function for the input capture interrupt.
 */
void kmTimer1InitInputCapture(const uint32_t iddleTimeMicroseconds,
							void *iddleUserData, kmTimer1CallbackType *iddleCallback,
							void *inpCaptureUserData, kmTimer1InpCaptureCallbackType *inpCaptureCallback);

// Setters callback user data
/**
Sets or changes User Data passed to the callback registered with #kmTimer1RegisterCallbackOVF.
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER1_USER_DATA macro to cast the data to correct format.\n
@param userData User Data as described above, example:\n

@code
	kmTimer1SetCallbackUserDataOVF(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_C));
@endcode
*/
void kmTimer1SetCallbackUserDataOVF(void *userData);

/**
Sets or changes User Data passed to the callback registered with #kmTimer1RegisterCallbackCompA.
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER1_USER_DATA macro to cast the data to correct format.\n
@param userData User Data as described above, example:\n

@code
	kmTimer1RegisterCallbackCompA(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_A));
@endcode
*/
void kmTimer1SetCallbackUserDataCompA(void *userData);

/**
Sets or changes User Data passed to the callback registered with #kmTimer1RegisterCallbackCompB.
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER1_USER_DATA macro to cast the data to correct format.\n
@param userData User Data as described above, example:\n

@code
	kmTimer1RegisterCallbackCompB(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_B));
@endcode
*/
void kmTimer1SetCallbackUserDataCompB(void *userData);

/**
Sets or changes User Data passed to the callback registered with #kmTimer1RegisterCallbackCompC.
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER1_USER_DATA macro to cast the data to correct format.\n
@param userData User Data as described above, example:\n

@code
	kmTimer1RegisterCallbackCompC(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_B));
@endcode
*/
void kmTimer1SetCallbackUserDataCompC(void *userData);


// Registering callbacks
/**
Functions to register callback launched on Timer1 Overflow. The callback needs to be declared with #kmTimer1CallbackType
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER1_USER_DATA macro to cast the data to correct format.\n
\b NOTE: The callback is unbuffered and is executed straight from the interrupt, so the routine should be as short as possible
@param userData User Data as described above
@param callback Callback declared with #kmTimer1CallbackType type

@code
void callbackOVFToggle(void *userData) {
	uint16_t a = (int16_t)userData; // example unwraping user data
	dbToggle(DB_PIN_0);
}

/// application routine
kmTimer1RegisterCallbackOVF(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_B), callbackOVFToggle);
@endcode
*/
void kmTimer1RegisterCallbackOVF(void *userData, kmTimer1CallbackType *callback);

/**
Functions to register callback launched on Timer1 Overflow Interrupt. The callback needs to be declared with #kmTimer1CallbackType
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER1_USER_DATA macro to cast the data to correct format.\n
\b NOTE: The callback is unbuffered and is executed straight from the interrupt, so the routine should be as short as possible
@param userData User Data as described above
@param callback Callback declared with #kmTimer1CallbackType type

@code
void callbackCompAToggle(void *userData) {
	uint16_t a = (int16_t)userData; // example unwraping user data
	dbToggle(DB_PIN_0);
}

/// application routine
kmTimer1RegisterCallbackCompA(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_A), callbackCompAToggle);
@endcode
*/
void kmTimer1RegisterCallbackCompA(void *userData, kmTimer1CallbackType *callback);

/**
Functions to register callback launched on Timer1 Overflow Interrupt. The callback needs to be declared with #kmTimer1CallbackType
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER1_USER_DATA macro to cast the data to correct format.\n
\b NOTE: The callback is unbuffered and is executed straight from the interrupt, so the routine should be as short as possible
@param userData User Data as described above
@param callback Callback declared with #kmTimer1CallbackType type

@code
void callbackCompBToggle(void *userData) {
	uint16_t a = (int16_t)userData; // example unwraping user data
	dbToggle(DB_PIN_1);
}

/// application routine
kmTimer1RegisterCallbackCompB(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_B), callbackCompBToggle);
@endcode
*/
void kmTimer1RegisterCallbackCompB(void *userData, kmTimer1CallbackType *callback);

/**
Functions to register callback launched on Timer1 Overflow Interrupt. The callback needs to be declared with #kmTimer1CallbackType
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER1_USER_DATA macro to cast the data to correct format.\n
\b NOTE: The callback is unbuffered and is executed straight from the interrupt, so the routine should be as short as possible
@param userData User Data as described above
@param callback Callback declared with #kmTimer1CallbackType type

@code
void callbackCompCToggle(void *userData) {
	uint16_t a = (int16_t)userData; // example unwraping user data
	dbToggle(DB_PIN_2);
}

/// application routine
kmTimer1RegisterCallbackCompA(KM_TIMER1_USER_DATA(KM_TIMER1_TEST_USER_DATA_C), callbackCompCToggle);
@endcode
*/
void kmTimer1RegisterCallbackCompC(void *userData, kmTimer1CallbackType *callback);

/**
@brief Register a callback function for Timer 1 input capture event.

This function registers a user-provided callback function that will be
called when a Timer 1 input capture event occurs. The user can also
provide a custom data pointer which will be passed back to the callback
when it is invoked.

@param[in] userData A pointer to user-defined data that will be passed to
                    the callback function.
@param[in] callback A pointer to the function that will be called on Timer 1
                    input capture event. This function must match the
                    signature defined by kmTimer5InpCaptureCallbackType.

@note The callback function must be set up before enabling the Timer 1 input
      capture to ensure that events are handled correctly.
*/
void kmTimer1RegisterCallbackInpCapture(void *userData, kmTimer1InpCaptureCallbackType *callback);

// Unregistering callbacks
/**
Permanently unregisters callback registered with #kmTimer1RegisterCallbackOVF function
*/
void kmTimer1UnregisterCallbackOVF(void);

/**
Permanently unregisters callback registered with #kmTimer1RegisterCallbackCompA function
*/
void kmTimer1UnregisterCallbackCompA(void);

/**
Permanently unregisters callback registered with #kmTimer1RegisterCallbackCompB function.
*/
void kmTimer1UnregisterCallbackCompB(void);

/**
Permanently unregisters callback registered with #kmTimer1RegisterCallbackCompC function.
*/
void kmTimer1UnregisterCallbackCompC(void);

/**
Permanently unregisters callback registered with #kmTimer1RegisterCallbackInpCapture function.
*/
void kmTimer1UnregisterCallbackInpCapture(void);

// Enabling interrupts
/**
Enables Timer1 Overflow interrupt allowing to execute routine registered with #kmTimer1RegisterCallbackOVF
*/
void kmTimer1EnableInterruptOVF(void);

/**
Enables Timer1 Comparator A interrupt allowing to execute routine registered with #kmTimer1RegisterCallbackCompA
*/
void kmTimer1EnableInterruptCompA(void);

/**
Enables Timer1 Comparator B interrupt allowing to execute routine registered with #kmTimer1RegisterCallbackCompB
*/
void kmTimer1EnableInterruptCompB(void);

#ifdef COM1C0
/**
Enables Timer1 Comparator C interrupt allowing to execute routine registered with #kmTimer1RegisterCallbackCompC
*/
void kmTimer1EnableInterruptCompC(void);
#endif /* COM1C0 */

/**
@brief Enables Timer/Counter 1 Input Capture Interrupt.

This function sets the Input Capture Interrupt Enable bit for Timer/Counter 1.
After calling this function, the timer will generate an interrupt when an input capture event occurs.
The specific input capture event conditions are determined by the timer configuration and mode.
Ensure that the corresponding input capture callback is registered using #kmTimer1RegisterCallbackInpCapture
before enabling the input capture interrupt.

Example usage:
@code
kmTimer1EnableInterruptInpCapture();
@endcode
 */
void kmTimer1EnableInterruptInpCapture(void);

// Disabling interrupts
/**
Disables all Timer 1 interrupts (Overflow, ComparatorA, ComparatorB, ComparatorC & Input Capture).
*/
void kmTimer1DisableInterruptsAll(void);

/**
Disables Timer 1 Overflow interrupt
*/
void kmTimer1DisableInterruptOVF(void);

/**
Disables Timer 1 ComparatorA interrupt
*/
void kmTimer1DisableInterruptCompA(void);

/**
Disables Timer 1 ComparatorB interrupt
*/
void kmTimer1DisableInterruptCompB(void);

/**
Disables Timer 1 ComparatorC interrupt
*/
void kmTimer1DisableInterruptCompC(void);

/**
@brief Sets the value for Timer/Counter 1 Overflow.

This function sets the value for Timer/Counter 1 Overflow. The timer counts from 0 to this value
before generating an overflow interrupt and resetting to zero. Ensure that the corresponding
overflow callback is registered using #kmTimer1RegisterCallbackOVF before setting the overflow value.

@param value The value to set for Timer/Counter 1 Overflow.

Example usage:
@code
kmTimer1SetValueOvf(65535); // Set overflow value to the maximum (16-bit)
@endcode
 */
void kmTimer1SetValueOvf(uint16_t value);

// Configuration of Comparator Outputs
/**
Configures Compare Output Mode channel A for Timer1 (\b OC1A output) and sets the pin assigned to this function as an output
@param compareOutputMode selects one of Compare Output Modes as defined in 
kmTimersCommon/kmTimer1Defs.h (e.g. \b KM_TCC1_A_COMP_OUT_TOGGLE)
*/
void kmTimer1ConfigureOCA(uint8_t compareOutputMode);

/**
Configures Compare Output Mode channel B for Timer1 (\b OC1B output) and sets the pin assigned to this function as an output
@param compareOutputMode selects one of Compare Output Modes as defined in 
kmTimersCommon/kmTimer1Defs.h (e.g. \b KM_TCC1_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN)
*/
void kmTimer1ConfigureOCB(uint8_t compareOutputMode);

#ifdef COM1C0
/**
Configures Compare Output Mode channel C for Timer1 (\b OC1C output) and sets the pin assigned to this function as an output
@param compareOutputMode selects one of Compare Output Modes as defined in
kmTimersCommon/kmTimer1Defs.h (e.g. \b KM_TCC1_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN)
*/
void kmTimer1ConfigureOCC(uint8_t compareOutputMode);
#endif /* COM1C0 */

// Controlling timer flow
/**
Enables running of Timer 1, allowing to execute Timer/Counter function defined with Initialization functions
*/
void kmTimer1Start(void);

/**
Stops running of Timer 1
*/
void kmTimer1Stop(void);

/**
Restarts internal counter of Timer 1.
*/
void kmTimer1Restart(void);

// Controlling PWM
/**
Sets the Duty in the PWM modes initialized as Bottom-To-Top. Can be applied to both OC1A and OC1B outputs
@param pwmOut either KM_TCC1_PWM_OUT_A for \b OC1A or KM_TCC1_PWM_OUT_B for \b OC1B
@param duty duty of the square wave generated on the OC1A/OC1B pin of the MCU; the range from 0 to 65535,
where 16384 means 25% and 32768 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle
*/

void kmTimer1SetPwmDutyBottomToTop(Tcc1PwmOut pwmOut, uint16_t duty);
/**
Sets the Duty in the PWM modes initialized as Accurate-Time. Can be applied to OC1B output only (as Comparator A is used to control general time period)
@param duty duty of the square wave generated on the OC1A/OC1B pin of the MCU; the range from 0 to 65535,
where 16384 means 25%/75% and 32768 means 50%/50% duty cycle; this initialization offers maximum accuracy of the duty cycle
The accuracy of the value depends on the main clock of the MCU and \e microseconds parameter provided in initialization function
The function calculates it in the way to be at least equal or higher than #KM_TCC1_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer1DefaultConfig
*/
void kmTimer1SetPwmDutyAccurateTimeModes(uint16_t duty);

/**
Function allows to invert PWM output either on \b OC1A or on \b OC1B
@param pwmOut either KM_TCC1_PWM_OUT_A for \b OC1A or KM_TCC1_PWM_OUT_B for \b B
@param inverted value true inverts the PWM output, normal operation when false OC2
*/
void kmTimer1SetPwmInversion(Tcc1PwmOut pwmOut, bool inverted);

// Timer flow calculations
/**
Function calculates prescaler and comparator cycles for Timer on basis of desired comparator match period\n

@param microseconds time period of the expected Comparator matches for Timer 1
the available range and accuracy depends on the main MCU clock as specified in the table below
\b NOTE: Some modes of Timer1 require to divide number of microseconds by 2, this happens for example for
Phase Correct modes, as Timer/Counter counts first up than down to get the proper result on the output.\n
@param prescaler result prescaler value as defined in \b kmTimersCommon\kmTimer1Defs.h to be used in #kmTimer1Init
@return comparator match value to be used in Timer initialization #kmTimer1Init function
*/
uint16_t kmTimer1CalcPerdiod(const uint32_t microseconds, uint8_t *prescaler);

/**
Function calculates prescaler and comparator cycles for Timer on basis of desired comparator match period. This version takes into consideration minim resolution of PWM duty and decreases frequency if needed\n
@param microseconds time period of the expected Comparator matches for Timer 1
the available range and accuracy depends on the main MCU clock as specified in the table below.
Note this version of the function takes into consideration desired minimum resolution of the output square wave duty.
In practice this may only matter for very low main clock frequencies. If the desired minimum resolution cannot be reached,
this function will return prescaler and comparator match value decreasing desired period defined in milliseconds
\b NOTE: Some modes of Timer1 require to divide number of microseconds by 2, this happens for example for
Phase Correct modes, as Timer/Counter counts first up than down to get the proper result on the output.
@param prescaler result prescaler value as defined in \b kmTimersCommon\kmTimer1Defs.h to be used in #kmTimer1Init
@param minimumCycleAcuracy desired minimum resolution of the output square wave duty (e.g. 4 means that at least 0%, 25%, 50% and 75% are achievable)
@return comparator match value to be used in Timer initialization #kmTimer1Init function
*/
uint16_t kmTimer1CalcPerdiodWithMinPwmAccuracy(const uint32_t microseconds, uint8_t *prescaler, const uint16_t minimumCycleAcuracy);

/**
Calculates compare match result based on desired square wave duty and available compare match cycles
calculated for Accurate-Time modes (e.g. got as result of #kmTimer1CalcPerdiod or #kmTimer1InitOnAccurateTimeFastPwm)
@param duty desired duty cycle
@param cyclesRange available range of cycles for duty cycle
@return comparator match value corresponding to desired duty cycle (can be used e.g. in #kmTimer1SetValueCompA)
*/
uint16_t kmTimer1CalcDutyOnCycles(uint16_t duty, uint16_t cyclesRange);


/**
@brief Retrieves the current value of Timer/Counter 1 Overflow.

This function returns the current value of Timer/Counter 1 Overflow, representing the number of cycles
before the timer overflows and triggers an overflow interrupt.

@return The current value of Timer/Counter 1 Overflow.

Example usage:
@code
uint16_t ovfValue = kmTimer1GetValueOvf();
@endcode
 */
uint16_t kmTimer1GetValueOvf(void);

/**
@brief Retrieves the current value of Timer/Counter 1 Compare A.

This function returns the current value of Timer/Counter 1 Compare A, representing the number of cycles
before the timer triggers a Compare A interrupt.

@return The current value of Timer/Counter 1 Compare A.

Example usage:
@code
uint16_t compAValue = kmTimer1GetValueCompA();
@endcode
 */
uint16_t kmTimer1GetValueCompA(void);

/**
@brief Retrieves the current value of Timer/Counter 1 Compare B.

This function returns the current value of Timer/Counter 1 Compare B, representing the number of cycles
before the timer triggers a Compare B interrupt.

@return The current value of Timer/Counter 1 Compare B.

Example usage:
@code
uint16_t compBValue = kmTimer1GetValueCompB();
@endcode
 */
uint16_t kmTimer1GetValueCompB(void);

/**
@brief Retrieves the current value of Timer/Counter 1 Compare C.

This function returns the current value of Timer/Counter 1 Compare C, representing the number of cycles
before the timer triggers a Compare C interrupt.

@return The current value of Timer/Counter 1 Compare C.

Example usage:
@code
uint16_t compCValue = kmTimer1GetValueCompC();
@endcode
 */
uint16_t kmTimer1GetValueCompC(void);

/**
@brief Sets the value for Timer/Counter 1 Overflow.

This function sets the value for Timer/Counter 1 Overflow. The timer counts from 0 to this value
before generating an overflow interrupt and resetting to zero. The value is also stored internally
for future reference.

@param value The value to set for Timer/Counter 1 Overflow.

Example usage:
@code
kmTimer1SetValueOvf(65535); // Set overflow value to the maximum (16-bit)
@endcode
 */
void kmTimer1SetValueOvf(uint16_t value);

/**
@brief Sets the value for Timer/Counter 1 Compare A.

This function sets the value for Timer/Counter 1 Compare A. The timer counts from 0 to this value
before triggering a Compare A interrupt. The value is also stored internally for future reference.

@param value The value to set for Timer/Counter 1 Compare A.

Example usage:
@code
kmTimer1SetValueCompA(5000); // Set Compare A value to 5000
@endcode
 */
void kmTimer1SetValueCompA(uint16_t value);

/**
@brief Sets the value for Timer/Counter 1 Compare B.

This function sets the value for Timer/Counter 1 Compare B. The timer counts from 0 to this value
before triggering a Compare B interrupt. The value is also stored internally for future reference.

@param value The value to set for Timer/Counter 1 Compare B.

Example usage:
@code
kmTimer1SetValueCompB(3000); // Set Compare B value to 3000
@endcode
 */
void kmTimer1SetValueCompB(uint16_t value);

#ifdef COM1C0
/**
@brief Sets the value for Timer/Counter 1 Compare C.

This function sets the value for Timer/Counter 1 Compare C. The timer counts from 0 to this value
before triggering a Compare C interrupt. The value is also stored internally for future reference.

@param value The value to set for Timer/Counter 1 Compare C.

Example usage:
@code
kmTimer1SetValueCompC(1000); // Set Compare C value to 1000
@endcode
 */
void kmTimer1SetValueCompC(uint16_t value);
#endif /* COM1C0 */

/**
@brief Gets the state of Timer/Counter 1 Input Capture pin.

This function returns the current state of the Timer/Counter 1 Input Capture pin.
It checks whether the Input Capture pin is currently high or low.

@return The state of the Timer/Counter 1 Input Capture pin:
        - \c true if the Input Capture pin is high.
        - \c false if the Input Capture pin is low.

Example usage:
@code
bool inpCaptureState = kmTimer1InpCaputureGetState();
if (inpCaptureState) {
    // Input Capture pin is high
} else {
    // Input Capture pin is low
}
@endcode
 */
bool kmTimer1InpCaputureGetState(void);

/**
@brief Initializes Timer 1 with the specified prescaler and modes.

This function initializes Timer 1 with the specified prescaler and modes.

@param prescaler The prescaler value to be set for Timer 1.
@param modeA The mode value to be set for Timer 1 control register A (TCCR1A).
@param modeB The mode value to be set for Timer 1 control register B (TCCR1B).

@note This function stops Timer 4 before initializing it.

@see kmTimer1Stop()
 */
void kmTimer1Init(const uint8_t prescaler, const uint8_t modeA, const uint8_t modeB);

/**
@brief Starts Timer 1 with the previously configured prescaler.

This function starts Timer 1 with the previously configured prescaler.
 */
void kmTimer1Start(void);

/**
@brief Stops Timer 1.

This function stops Timer 1.
 */
void kmTimer1Stop(void);

/**
@brief Restarts Timer 4 by resetting its counter value to zero.

This function restarts Timer 4 by resetting its counter value to zero.
 */
void kmTimer1Restart(void);

#ifdef __cplusplus
}
#endif /*  __cplusplus */
#endif /* KM_TIMER_1_H_ */
