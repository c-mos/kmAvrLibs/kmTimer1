# Readme
# kmTimer1 library for AVR MCUs

The kmTimer1 library provides functions to initialize and control Timer 1 on AVR MCUs. It allows generating square waves with specified duty cycle and frequency, registering callbacks for overflow and comparator interrupts, enabling/disabling interrupts, configuring comparator outputs, controlling timer flow, and setting PWM duty cycles.

## Table of Contents
- [Version History](#version-history)
- [Overview](#overview)
- [Dependencies](#dependencies)
- [Usage](#usage)
- [Author and License](#author-and-license)

## Version History
v1.0.0 Initial (2024-07-06)

## Overview
This library is designed for older models of AVR MCUs like ATmega8, ATmega32, etc. It initializes Timer 1 to generate square waves using Phase Accurate mode, allowing precise control over duty cycle and frequency. Callback functions can be registered for overflow and comparator interrupts, enabling users to execute custom routines at specific moments within Timer 1 periods.

## Dependencies
- [kmFrameworkAVR](https://gitlab.com/c-mos/kmAvrLibs)
  - [kmCommon](https://gitlab.com/c-mos/kmAvrLibs/kmCommon)
  - [kmCpu](https://gitlab.com/c-mos/kmAvrLibs/kmCpu)
  - [kmTimersCommon](https://gitlab.com/c-mos/kmAvrLibs/kmTimersCommon)

## Usage

Getting this library and adding it to own project:

- To add this module to own project as submodule - enter the main directory of the source code and use git command

``` bash
git submodule add git@gitlab.com:c-mos/kmAvrLibs/kmTimer1.git kmTimer1
```

- After cloning own application from git repository use following additional git command to get correct revision of submodule:
``` bash
git submodule update --init
```

See Also:
[kmTimer1 Example Test Application](https://gitlab.com/c-mos/kmAvrTests/kmTimer1Test)

## Author and License
**Author**: Krzysztof Moskwa

**e-mail**: chris[dot]moskva[at]gmail[dot]com

**License**: GPL-3.0-or-later

Software License: GNU General Public License (GPL) version 3.0 or later. See [LICENSE.txt](https://www.gnu.org/licenses/gpl-3.0.txt)

 ![GPL3 Logo](https://www.gnu.org/graphics/gplv3-or-later-sm.png)
