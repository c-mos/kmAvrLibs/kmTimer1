/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//*
* kmTimer1.c
*
*  **Created on**: May 03, 2022 @n
*      Author: Krzysztof Moskwa
*      License: GPL-3.0-or-later
*
*  kmDebug library for AVR MCUs
*  Copyright (C) 2019  Krzysztof Moskwa
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/

#include "kmTimer1.h"

#include <stdlib.h>
#include <stdbool.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/atomic.h>

#include "../kmTimersCommon/kmTimer1Defs.h"


// Definitions
#define KM_TCC1_TOP KM_TCC_TOP_3

#define KM_TCC1_CYCLES_CALCULATION_CORRECTION 1u

// "Private" variables
static uint8_t _kmTimer1PrescalerSelectBits = 0;

static uint16_t _kmTimer1CompOvfCycles = KM_TCC1_TOP;

static uint16_t _kmTimer1CompACycles = KM_TCC1_TOP;
static bool _kmTimer1OtputComparePinUsedA = false;

static uint16_t _kmTimer1CompBCycles = KM_TCC1_TOP;
static bool _kmTimer1OtputComparePinUsedB = false;

#ifdef COM1C0
static uint16_t _kmTimer1CompCCycles = KM_TCC1_TOP;
static bool _kmTimer1OtputComparePinUsedC = false;
#endif /* COM1C0 */

static kmTimer1CallbackType *_kmTimer1CallbackOVF = NULL;
static void *_kmTimer1CallbackUserDataOVF = NULL;

static kmTimer1CallbackType *_kmTimer1CallbackCompA = NULL;
static void *_kmTimer1CallbackUserDataCompA = NULL;

static kmTimer1CallbackType *_kmTimer1CallbackCompB = NULL;
static void *_kmTimer1CallbackUserDataCompB = NULL;

#ifdef COM1C0
static kmTimer1CallbackType *_kmTimer1CallbackCompC = NULL;
static void *_kmTimer1CallbackUserDataCompC = NULL;
#endif /* COM1C0 */

static kmTimer1InpCaptureCallbackType *_kmTimer1CallbackInpCapture = NULL;
static void *_kmTimer1CallbackUserDataInpCapture = NULL;
static uint16_t _inpCaptureCycles = 0;

// "Private" functions

// "Public" functions
// Controlling timer flow
void kmTimer1Start(void);
void kmTimer1Stop(void);
void kmTimer1Restart(void);

// Controlling PWM
void kmTimer1SetPwmDutyBottomToTop(Tcc1PwmOut pwmOut, uint16_t duty);
void kmTimer1SetPwmInversion(Tcc1PwmOut pwmOut, bool inverted);

// Timer flow calculations
uint16_t kmTimer1CalcPerdiod(const uint32_t microseconds, uint8_t *prescaler);
uint16_t kmTimer1CalcPerdiodWithMinPwmAccuracy(const uint32_t microseconds, uint8_t *prescaler, const uint16_t minimumCycleAcuracy);
uint16_t kmTimer1CalcDutyOnCycles(uint16_t duty, uint16_t cycles);

// Implementation
void kmTimer1Init(const uint8_t prescaler, const uint8_t modeA, const uint8_t modeB) {
	kmTimer1Stop();
	_kmTimer1PrescalerSelectBits = prescaler;
	TCCR1A = (TCCR1A & ~KM_TCC1_MODE_MASK_A) | modeA;
	TCCR1B = (TCCR1B & ~KM_TCC1_MODE_MASK_B) | modeB;
}

void kmTimer1InitOnPrescalerBottomToTopOvfCompABCInterruptCallback(const uint8_t prescaler) {
	kmTimer1Init(prescaler, KM_TCC1_MODE_0_A, KM_TCC1_MODE_0_B);
}

void kmTimer1InitOnAccuratePeriodGenerateOutputClockA(const uint32_t periodInMicroseconds) {
	kmTimer1InitOnAccurateTimeCompAInterruptCallback(periodInMicroseconds >> KMC_DIV_BY_2, true);
}

void kmTimer1InitOnAccurateTimeCompAInterruptCallback(const uint32_t microseconds, const bool outOnOC1A) {
	_kmTimer1CompACycles = kmTimer1CalcPerdiod(microseconds, &_kmTimer1PrescalerSelectBits);
	kmTimer1Init(_kmTimer1PrescalerSelectBits, KM_TCC1_MODE_4_A, KM_TCC1_MODE_4_B);
	kmTimer1SetValueCompA(_kmTimer1CompACycles);
	kmTimer1ConfigureOCA(outOnOC1A ? KM_TCC1_A_PWM_COMP_OUT_TOGGLE : KM_TCC1_A_PWM_COMP_OUT_NORMAL);
}

void kmTimer1InitOnAccurateTimeCompABInterruptCallback(const uint32_t microseconds, uint16_t phaseIntB) {
	_kmTimer1CompACycles = kmTimer1CalcPerdiod(microseconds, &_kmTimer1PrescalerSelectBits);
	_kmTimer1CompBCycles = kmTimer1CalcDutyOnCycles(phaseIntB, _kmTimer1CompACycles);
	kmTimer1Init(_kmTimer1PrescalerSelectBits, KM_TCC1_MODE_4_A, KM_TCC1_MODE_4_B);
	kmTimer1SetValueCompA(_kmTimer1CompACycles);
	kmTimer1SetValueCompB(_kmTimer1CompBCycles);
}

void kmTimer1InitOnPrescalerBottomToTopFastPwmOCA(const uint8_t prescaler
												, const uint16_t dutyA
												, const bool invertedA) {
	kmTimer1Init(prescaler, KM_TCC1_MODE_E_A, KM_TCC1_MODE_E_B);
	kmTimer1SetValueOvf(_kmTimer1CompOvfCycles);

	if (dutyA > KMC_UNSIGNED_ZERO) {
		kmTimer1ConfigureOCA(invertedA ? KM_TCC1_A_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC1_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
		kmTimer1SetValueCompA(dutyA);
	}
}

void kmTimer1InitOnPrescalerBottomToTopFastPwm(const uint8_t prescaler
											, const uint16_t dutyA
											, const bool invertedA
											, const uint16_t dutyB
											, const bool invertedB
#ifdef COM3C0
											, const uint16_t dutyC
											, const bool invertedC
#endif /* COM3C0 */
											) {
	kmTimer1InitOnPrescalerBottomToTopFastPwmOCA(prescaler, dutyA, invertedA);

	if (dutyB > KMC_UNSIGNED_ZERO) {
		kmTimer1ConfigureOCB(invertedB ? KM_TCC1_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC1_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
		kmTimer1SetValueCompB(dutyB);
	}

#ifdef COM1C0
	if (dutyC > KMC_UNSIGNED_ZERO) {
		kmTimer1ConfigureOCC(invertedC ? KM_TCC1_C_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC1_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
		kmTimer1SetValueCompC(dutyC);
	}
#endif /* COM1C0 */
}

uint16_t kmTimer1InitOnAccurateTimeFastPwm(const uint32_t microseconds
											, const uint16_t dutyA
											, const bool invertedA
											, const uint16_t dutyB
											, const bool invertedB
#ifdef COM3C0
											, const uint16_t dutyC
											, const bool invertedC
#endif /* COM3C0 */
											) {
	
	_kmTimer1CompOvfCycles = kmTimer1CalcPerdiodWithMinPwmAccuracy(microseconds, &_kmTimer1PrescalerSelectBits, KM_TCC1_MINIMUM_PWM_CYCCLE_ACCURACY);
	kmTimer1Init(_kmTimer1PrescalerSelectBits, KM_TCC1_MODE_E_A, KM_TCC1_MODE_E_B);
	kmTimer1SetValueOvf(_kmTimer1CompOvfCycles);

	if (dutyA > KMC_UNSIGNED_ZERO) {
		_kmTimer1CompACycles = kmTimer1CalcDutyOnCycles(dutyA, _kmTimer1CompOvfCycles);
		kmTimer1SetValueCompA(_kmTimer1CompACycles);
		kmTimer1ConfigureOCA(invertedA ? KM_TCC1_A_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC1_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}

	if (dutyB > KMC_UNSIGNED_ZERO) {
		_kmTimer1CompBCycles = kmTimer1CalcDutyOnCycles(dutyB, _kmTimer1CompOvfCycles);
		kmTimer1SetValueCompB(_kmTimer1CompBCycles);
		kmTimer1ConfigureOCB(invertedB ? KM_TCC1_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC1_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}

#ifdef COM1C0
	if (dutyC > KMC_UNSIGNED_ZERO) {
		_kmTimer1CompCCycles = kmTimer1CalcDutyOnCycles(dutyC, _kmTimer1CompOvfCycles);
		kmTimer1SetValueCompC(_kmTimer1CompCCycles);
		kmTimer1ConfigureOCC(invertedC ? KM_TCC1_C_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC1_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}
#endif /* COM1C0 */

	return _kmTimer1CompOvfCycles;
}

void kmTimer1InitOnPrescalerBottomToTopPcPwmOCA(const uint8_t prescaler
												, const uint16_t dutyA
												, const bool invertedA) {
	kmTimer1Init(prescaler, KM_TCC1_MODE_A_A, KM_TCC1_MODE_A_B);
	kmTimer1SetValueOvf(_kmTimer1CompOvfCycles);

	kmTimer1SetValueCompA(dutyA);
	kmTimer1ConfigureOCA(invertedA ? KM_TCC1_A_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC1_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
}

void kmTimer1InitOnPrescalerBottomToTopPcPwm(const uint8_t prescaler
											, const uint16_t dutyA
											, const bool invertedA
											, const uint16_t dutyB
											, const bool invertedB
#ifdef COM3C0
											, const uint16_t dutyC
											, const bool invertedC
#endif /* COM3C0 */
											) {
	kmTimer1InitOnPrescalerBottomToTopPcPwmOCA(prescaler, dutyA, invertedA);

	if (dutyB > 0) {
		kmTimer1SetValueCompB(dutyB);
		kmTimer1ConfigureOCB(invertedB ? KM_TCC1_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC1_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}

#ifdef COM1C0
	if (dutyC > 0) {
		kmTimer1SetValueCompC(dutyC);
		kmTimer1ConfigureOCC(invertedC ? KM_TCC1_C_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC1_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}
#endif /* COM1C0 */
}

uint16_t kmTimer1InitOnAccurateTimePcPwm(const uint32_t microseconds
										, const uint16_t dutyA
										, const bool invertedA
										, const uint16_t dutyB
										, const bool invertedB
#ifdef COM3C0
										, const uint16_t dutyC
										, const bool invertedC
#endif /* COM3C0 */
										) {
	_kmTimer1CompOvfCycles = kmTimer1CalcPerdiodWithMinPwmAccuracy(microseconds >> KMC_DIV_BY_2, &_kmTimer1PrescalerSelectBits, KM_TCC1_MINIMUM_PWM_CYCCLE_ACCURACY);
	kmTimer1SetValueOvf(_kmTimer1CompOvfCycles);
	kmTimer1Init(_kmTimer1PrescalerSelectBits, KM_TCC1_MODE_A_A, KM_TCC1_MODE_A_B);
	
	if (dutyB > KMC_UNSIGNED_ZERO) {
		_kmTimer1CompACycles = kmTimer1CalcDutyOnCycles(dutyA, _kmTimer1CompOvfCycles);
		kmTimer1SetValueCompA(_kmTimer1CompACycles);
		kmTimer1ConfigureOCA(invertedB ? KM_TCC1_A_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC1_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}

	if (dutyB > KMC_UNSIGNED_ZERO) {
		_kmTimer1CompBCycles = kmTimer1CalcDutyOnCycles(dutyB, _kmTimer1CompOvfCycles);
		kmTimer1SetValueCompB(_kmTimer1CompBCycles);
		kmTimer1ConfigureOCB(invertedB ? KM_TCC1_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC1_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}

#ifdef COM1C0
	if (dutyC > KMC_UNSIGNED_ZERO) {
		_kmTimer1CompCCycles = kmTimer1CalcDutyOnCycles(dutyC, _kmTimer1CompOvfCycles);
		kmTimer1SetValueCompC(_kmTimer1CompCCycles);
		kmTimer1ConfigureOCC(invertedC ? KM_TCC1_C_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC1_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}
#endif /* COM1C0 */

	return _kmTimer1CompOvfCycles;
}

void kmTimer1InitExternal(bool falling) {
	if (true == falling) {
		_kmTimer1PrescalerSelectBits = KM_TCC1_EXT_T1_FAL;
	} else {
		_kmTimer1PrescalerSelectBits = KM_TCC1_EXT_T1_RIS;
	}
}

void kmTimer1InitInputCapture(const uint32_t iddleTimeMicroseconds,
							void *iddleUserData, kmTimer1CallbackType *iddleCallback,
							void *inpCaptureUserData, kmTimer1InpCaptureCallbackType *inpCaptureCallback) {
	KM_TCC1_PWM_DDR &= ~_BV(KM_TCC1_T1_PIN);
	KM_TCC1_PWM_PORT |= _BV(KM_TCC1_T1_PIN);

	kmTimer1InitOnAccurateTimeCompAInterruptCallback(iddleTimeMicroseconds, false);

	kmTimer1RegisterCallbackCompA(KM_TIMER1_USER_DATA(iddleUserData), iddleCallback);
	kmTimer1RegisterCallbackInpCapture(KM_TIMER1_USER_DATA(inpCaptureUserData), inpCaptureCallback);
	kmTimer1EnableInterruptCompA();
	kmTimer1EnableInterruptInpCapture();

	kmTimer1Start();
}

uint16_t kmTimer1CalcDutyOnCycles(uint16_t duty, uint16_t cyclesRange) {
	uint32_t result = ((uint32_t)cyclesRange * (uint32_t)duty) / (uint32_t)KM_TCC1_TOP;
	return (uint16_t)result;
}

void kmTimer1SetPwmDutyAccurateTimeModes(uint16_t duty) {
	kmTimer1SetValueCompB(kmTimer1CalcDutyOnCycles(duty, _kmTimer1CompACycles));
}

void kmTimer1SetPwmDutyBottomToTop(Tcc1PwmOut pwmOut, uint16_t duty) {
	switch (pwmOut) {
		case KM_TCC1_PWM_OUT_A: {
			kmTimer1SetValueCompA(duty);
			break;
		}
		case KM_TCC1_PWM_OUT_B: {
			kmTimer1SetValueCompB(duty);
			break;
		}
#ifdef COM1C0
		case KM_TCC1_PWM_OUT_C: {
			kmTimer1SetValueCompC(duty);
			break;
		}
#endif /* COM1C0 */
		// no default
	}
}

uint16_t kmTimer1CalcPerdiod(const uint32_t microseconds, uint8_t *prescaler) {
	uint64_t cycles = (int64_t)(F_CPU);
	cycles *= microseconds;
	cycles /= KMC_CONV_MICROSECONDS_TO_SECONCS;
	if (cycles <= KM_TCC1_TOP) {
		// no prescaler, full XTAL
		*prescaler = KM_TCC1_PRSC_1;
	} else if ((cycles >>= KMC_DIV_BY_8) <= KM_TCC1_TOP) {
		// prescaler by /8
		*prescaler = KM_TCC1_PRSC_8;
	} else if ((cycles >>= KMC_DIV_BY_8) <= KM_TCC1_TOP) {
		// prescaler by /64
		*prescaler = KM_TCC1_PRSC_64;
	} else if ((cycles >>= KMC_DIV_BY_4) <= KM_TCC1_TOP) {
		// prescaler by /256
		*prescaler = KM_TCC1_PRSC_256;
	} else if ((cycles >>= KMC_DIV_BY_4) <= KM_TCC1_TOP) {
		// prescaler by /1024
		*prescaler = KM_TCC1_PRSC_1024;
	} else {
		// request was out of bounds, set as maximum
		*prescaler = KM_TCC1_PRSC_1024;
		cycles = KM_TCC1_TOP;
	}
	return (uint16_t)(cycles != KMC_UNSIGNED_ZERO ? cycles - KM_TCC1_CYCLES_CALCULATION_CORRECTION : cycles);
}

uint16_t kmTimer1CalcPerdiodWithMinPwmAccuracy(const uint32_t microseconds, uint8_t *prescaler, const uint16_t minimumCycleAcuracy) {
	uint16_t result = kmTimer1CalcPerdiod(microseconds, prescaler);
	
	if (result < minimumCycleAcuracy) {
		result = minimumCycleAcuracy;
	}
	
	return result;
}

void kmTimer1SetPwmInversion(Tcc1PwmOut pwmOut, bool inverted) {
	switch (pwmOut) {
		case KM_TCC1_PWM_OUT_A: {
			if (false == inverted) {
				kmTimer1ConfigureOCA(KM_TCC1_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
			} else {
				kmTimer1ConfigureOCA(KM_TCC1_A_PWM_COMP_OUT_SET_UP_CLEAR_DOWN);
			}
			break;
		}
		case KM_TCC1_PWM_OUT_B: {
			if (false == inverted) {
				kmTimer1ConfigureOCB(KM_TCC1_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
			} else {
				kmTimer1ConfigureOCB(KM_TCC1_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN);
			}
			break;
		}
#ifdef COM1C0
		case KM_TCC1_PWM_OUT_C: {
			if (false == inverted) {
				kmTimer1ConfigureOCC(KM_TCC1_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
			} else {
				kmTimer1ConfigureOCC(KM_TCC1_C_PWM_COMP_OUT_SET_UP_CLEAR_DOWN);
			}
			break;
		}
#endif /* COM1C0 */
		default: {
			// intentionally
		}
	}
}

void kmTimer1Start(void) {
	TCCR1B = (TCCR1B & ~KM_TCC1_CS_MASK) | _kmTimer1PrescalerSelectBits;
}

void kmTimer1Stop(void) {
	TCCR1B &= (TCCR1B & ~KM_TCC1_CS_MASK) | KM_TCC1_STOP;
}

void kmTimer1Restart(void) {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		TCNT1 = KMC_UNSIGNED_ZERO;
	}
}

void kmTimer1ConfigureOCA(uint8_t compareOutputMode) {
	// Do not touch pin configuration if NORMAL mode to be used and other mode never used
	if (KM_TCC1_A_PWM_COMP_OUT_NORMAL == compareOutputMode && false == _kmTimer1OtputComparePinUsedA) {
		return;
	}
	
	KM_TCC1_PWM_DDR |= KM_TCC1_PWM_BV_A;		// setup port for output
	KM_TCC1_PWM_PORT |= KM_TCC1_PWM_BV_A;		// HIGH by default

	TCCR1A = (TCCR1A & ~KM_TCC1_COMP_A_MASK) | compareOutputMode;

	_kmTimer1OtputComparePinUsedA = true;
}

void kmTimer1ConfigureOCB(uint8_t compareOutputMode) {
	// Do not touch pin configuration if NORMAL mode to be used and other mode never used
	if (KM_TCC1_B_PWM_COMP_OUT_NORMAL == compareOutputMode && false == _kmTimer1OtputComparePinUsedB) {
		return;
	}
	KM_TCC1_PWM_DDR |= KM_TCC1_PWM_BV_B;		// setup port for output
	KM_TCC1_PWM_PORT |= KM_TCC1_PWM_BV_B;		// HIGH by default

	TCCR1A = (TCCR1A & ~KM_TCC1_COMP_B_MASK) | compareOutputMode;

	_kmTimer1OtputComparePinUsedB = true;
}

#ifdef COM1C0
void kmTimer1ConfigureOCC(uint8_t compareOutputMode) {
	// Do not touch pin configuration if NORMAL mode to be used and other mode never used
	if (KM_TCC1_C_PWM_COMP_OUT_NORMAL == compareOutputMode && false == _kmTimer1OtputComparePinUsedC) {
		return;
	}
	KM_TCC1_PWM_DDR |= KM_TCC1_PWM_BV_C;		// setup port for output
	KM_TCC1_PWM_PORT |= KM_TCC1_PWM_BV_C;		// HIGH by default

	TCCR1A = (TCCR1A & ~KM_TCC1_COMP_C_MASK) | compareOutputMode;

	_kmTimer1OtputComparePinUsedC = true;
}
#endif /* COM1C0 */

void kmTimer1SetValueOvf(uint16_t value) {
	ICR1 = _kmTimer1CompOvfCycles = value; // -V2561
}

void kmTimer1SetValueCompA(uint16_t value) {
	OCR1A = _kmTimer1CompACycles = value; // -V2561
}

void kmTimer1SetValueCompB(uint16_t value) {
	OCR1B = _kmTimer1CompBCycles = value; // -V2561
}

#ifdef COM1C0
void kmTimer1SetValueCompC(uint16_t value) {
	OCR1C = _kmTimer1CompCCycles = value; // -V2561
}
#endif /* COM1C0 */

uint16_t kmTimer1GetValueOvf(void) {
	return _kmTimer1CompOvfCycles;
}

uint16_t kmTimer1GetValueCompA(void) {
	return _kmTimer1CompACycles;
}

uint16_t kmTimer1GetValueCompB(void) {
	return _kmTimer1CompBCycles;
}


#ifdef COM1C0
uint16_t kmTimer1GetValueCompC(void) {
	return _kmTimer1CompCCycles;
}
#endif /* COM1C0 */

void kmTimer1SetPrescale(uint8_t prescaler) {
	_kmTimer1PrescalerSelectBits = prescaler;
	kmTimer1Start();
}

void kmTimer1EnableInterruptCompA(void) {
	TIMSK1 |= _BV(OCIE1A);
}

void kmTimer1EnableInterruptCompB(void) {
	TIMSK1 |= _BV(OCIE1B);
}

#ifdef COM1C0
void kmTimer1EnableInterruptCompC(void) {
	TIMSK1 |= _BV(OCIE1C);
}
#endif /* COM1C0 */

void kmTimer1EnableInterruptOVF(void) {
	TIMSK1 |= _BV(TOIE1);
}

void kmTimer1EnableInterruptInpCapture(void) {
	TIMSK1 |= _BV(ICIE1);
}

void kmTimer1DisableInterruptCompA(void) {
	TIMSK1 &= ~_BV(OCIE1A);
}

void kmTimer1DisableInterruptCompB(void) {
	TIMSK1 &= ~_BV(OCIE1B);
}

#ifdef COM1C0
void kmTimer1DisableInterruptCompC(void) {
	TIMSK1 &= ~_BV(OCIE1C);
}
#endif /* COM1C0 */

void kmTimer1DisableInterruptOVF(void) {
	TIMSK1 &= ~_BV(TOIE1);
}

void kmTimer1DisableInterruptInpCapture(void) {
	TIMSK1 &= ~_BV(ICIE1);
}

void kmTimer1DisableInterruptsAll(void) {
#ifdef COM1C0
	TIMSK1 &= ~(_BV(OCIE1C) | _BV(OCIE1B) | _BV(OCIE1A) | _BV(TOIE1 | _BV(ICIE1)));
#else
	TIMSK1 &= ~( _BV(OCIE1B) | _BV(OCIE1A) | _BV(TOIE1 | _BV(ICIE1)));
#endif /* COM1C0 */
}

void kmTimer1RegisterCallbackCompA(void *userData, kmTimer1CallbackType *callback) {
	_kmTimer1CallbackCompA = callback;
	_kmTimer1CallbackUserDataCompA = userData;
}

void kmTimer1RegisterCallbackCompB(void *userData, kmTimer1CallbackType *callback) {
	_kmTimer1CallbackCompB = callback;
	_kmTimer1CallbackUserDataCompB = userData;
}

#ifdef COM1C0
void kmTimer1RegisterCallbackCompC(void *userData, kmTimer1CallbackType *callback) {
	_kmTimer1CallbackCompC = callback;
	_kmTimer1CallbackUserDataCompC = userData;
}
#endif /* COM1C0 */

void kmTimer1RegisterCallbackOVF(void *userData, kmTimer1CallbackType *callback) {
	_kmTimer1CallbackOVF = callback;
	_kmTimer1CallbackUserDataOVF = userData;
}

void kmTimer1RegisterCallbackInpCapture(void *userData, kmTimer1InpCaptureCallbackType *callback) {
	_kmTimer1CallbackInpCapture = callback;
	_kmTimer1CallbackUserDataInpCapture = userData;
}

void kmTimer1UnregisterCallbackCompA(void) {
	_kmTimer1CallbackUserDataCompA = NULL;
}

void kmTimer1UnregisterCallbackCompB(void) {
	_kmTimer1CallbackUserDataCompB = NULL;
}

#ifdef COM1C0
void kmTimer1UnregisterCallbackCompC(void) {
	_kmTimer1CallbackUserDataCompC = NULL;
}
#endif /* COM1C0 */

void kmTimer1UnregisterCallbackOVF(void) {
	_kmTimer1CallbackUserDataOVF = NULL;
}

void kmTimer1UnregisterCallbackInpCapture(void) {
	_kmTimer1CallbackInpCapture = NULL;
}

void kmTimer1SetCallbackUserDataOVF(void *userData)  {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		_kmTimer1CallbackUserDataOVF = userData;
	}
}

void kmTimer1SetCallbackUserDataInpCapture(void *userData)  {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		_kmTimer1CallbackUserDataInpCapture = userData;
	}
}

void kmTimer1SetCallbackUserDataCompA(void *userData)  {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		_kmTimer1CallbackUserDataCompA = userData;
	}
}

void kmTimer1SetCallbackUserDataCompB(void *userData)  {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		_kmTimer1CallbackUserDataCompB = userData;
	}
}

#ifdef COM1C0
void kmTimer1SetCallbackUserDataCompC(void *userData)  {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		_kmTimer1CallbackUserDataCompC = userData;
	}
}
#endif /* COM1C0 */

bool kmTimer1InpCaputureGetState(void) {
	return (KM_TCC1_ICP1_PORT_INPUT & _BV(KM_TCC1_ICP1_PIN));
}

ISR(TIMER1_COMPA_vect) {
	if (NULL != _kmTimer1CallbackCompA) {
		_kmTimer1CallbackCompA(_kmTimer1CallbackUserDataCompA);
	}
}

ISR(TIMER1_COMPB_vect) {
	if (NULL != _kmTimer1CallbackCompB) {
		_kmTimer1CallbackCompB(_kmTimer1CallbackUserDataCompB);
	}
}

#ifdef COM1C0
ISR(TIMER1_COMPC_vect) {
	if (NULL != _kmTimer1CallbackCompC) {
		_kmTimer1CallbackCompC(_kmTimer1CallbackUserDataCompC);
	}
}
#endif /* COM1C0 */

ISR(TIMER1_OVF_vect) {
	if (NULL != _kmTimer1CallbackOVF) {
		_kmTimer1CallbackOVF(_kmTimer1CallbackUserDataOVF);
	}
}

ISR(TIMER1_CAPT_vect) {
	_inpCaptureCycles = ICR1;
	TCCR1B ^= _BV(ICES1);
	kmTimer1Restart();
	if (NULL != _kmTimer1CallbackInpCapture) {
		_kmTimer1CallbackInpCapture(_inpCaptureCycles, kmTimer1InpCaputureGetState(), _kmTimer1CallbackUserDataInpCapture);
	}
}
